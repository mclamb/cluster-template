#!/bin/bash
set -euxo pipefail

export KUBECTL="kubectl --kubeconfig /root/.kube/config"

function allow_root_login_for_cloud_init
{
	# We need to login via terraform remote-exec from CI without keys
	sed -i 's@^PasswordAuthentication no$@PasswordAuthentication yes@' /etc/ssh/sshd_config
	sed -i 's@^#PermitRootLogin prohibit-password@PermitRootLogin yes@' /etc/ssh/sshd_config
	service ssh restart
}

function install_kube_tools
{
	echo "Installing Kubeadm tools..."
	mkdir -p /root/{.kube,kube/ceph}
	swapoff -a
	apt update && apt-get install -y apt-transport-https
	curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
	echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
	apt update && apt-get install -y kubectl
}

function install_docker
{
	echo "Installing and enabling Docker..."
	apt install -y docker.io
	cat <<-EOF > /etc/docker/daemon.json
	{
		"exec-opts": ["native.cgroupdriver=systemd"]
	}
	EOF

	systemctl enable docker
	systemctl start docker
}

function connect_to_rancher
{
	echo "Connecting to Rancher server in order to deploy RKE"
	if [[ $(hostname) =~ "controller" ]]; then
		${node_command} --controlplane --etcd --worker
	else
		${node_command} --worker
	fi
	set +x
	state="provisioning"
	while [[ $${state} != "active" ]]; do
		echo -n "Checking if cluster ${rancher2_api_url}/clusters/${rancher2_cluster_id} is ready..."
		state=$(curl -u "${rancher2_access_key}:${rancher2_secret_key}" \
			${rancher2_api_url}/clusters/${rancher2_cluster_id} 2>/dev/null | jq -r '.state')
		if [[ $${state} != "active" ]]; then
			echo "not yet. Sleeping for 5 seconds..."
			sleep 5
		fi
	done
	echo "IT'S READY!"
	set -x

	sleep 10
}

function get_kubeconfig
{
	echo "Retrieving kubeconfig from cluster..."
	kubectl --kubeconfig $(docker inspect kubelet --format \
		'{{ range .Mounts }}{{ if eq .Destination "/etc/kubernetes" }}{{ .Source }}{{ end }}{{ end }}')/ssl/kubecfg-kube-node.yaml \
		get configmap -n kube-system full-cluster-state -o json | jq -r .data.\"full-cluster-state\" | \
		jq -r .currentState.certificatesBundle.\"kube-admin\".config | \
		sed -e "/^[[:space:]]*server:/ s_:.*_: \"https://127.0.0.1:6443\"_" > /root/.kube/config
	sleep 10
	$${KUBECTL} get nodes
}

function deploy_packet_csi
{
	if [ "${deploy_packet_csi}" = "no" ]; then
		return
	fi

	echo "Deploying the Packet CSI Storage Class..."

	cat <<-EOF > /root/kube/packet-config.yaml
	apiVersion: v1
	kind: Secret
	metadata:
	  name: packet-cloud-config
	  namespace: kube-system
	stringData:
	  cloud-sa.json: |
	    {
	      "apiKey": "${packet_auth_token}",
	      "projectID": "${packet_project_id}"
	    }
	EOF

	$${KUBECTL} apply -f /root/kube/packet-config.yaml
	$${KUBECTL} apply -f https://raw.githubusercontent.com/packethost/csi-packet/master/deploy/kubernetes/setup.yaml
	$${KUBECTL} apply -f https://raw.githubusercontent.com/packethost/csi-packet/master/deploy/kubernetes/node.yaml
	$${KUBECTL} apply -f https://raw.githubusercontent.com/packethost/csi-packet/master/deploy/kubernetes/controller.yaml

	# Remove Packet CSI as the default StorageClass
	$${KUBECTL} patch storageclass csi-packet-standard -p \
		'{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'

}

function deploy_metal_lb
{
	if [ "${deploy_metal_lb}" = "no" ]; then
		return
	fi

	echo "Deploying the Metal LB bare-metal LoadBalancer for ${packet_network_cidr}..."

	cat <<-EOF > /root/kube/metal_lb.yaml
	apiVersion: v1
	kind: ConfigMap
	metadata:
	  namespace: metallb-system
	  name: config
	data:
	  config: |
	    address-pools:
	    - name: packet-network
	      protocol: layer2
	      addresses:
	      - ${packet_network_cidr}
	EOF

	$${KUBECTL} apply -f https://raw.githubusercontent.com/google/metallb/v${metal_lb_version}/manifests/metallb.yaml
	$${KUBECTL} apply -f /root/kube/metal_lb.yaml

	echo "Moving metallb-system namespace into Rancher System project..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d '{"projectId":"'${rancher2_system_project_id}'"}' \
		${rancher2_api_url}/cluster/${rancher2_cluster_id}/namespaces/metallb-system?action=move 2>/dev/null
}

function deploy_ceph
{
	if [ "${deploy_ceph}" = "no" ]; then
		return
	fi

	echo "Deploying Ceph..."

	cd /root/kube/ceph

	wget https://raw.githubusercontent.com/rook/rook/v${rook_version}/cluster/examples/kubernetes/ceph/common.yaml
	$${KUBECTL} apply -f common.yaml
	sleep 30

	wget https://raw.githubusercontent.com/rook/rook/v${rook_version}/cluster/examples/kubernetes/ceph/operator.yaml
	$${KUBECTL} apply -f operator.yaml
	sleep 30

	if [ $((${num_controller_worker_nodes} + ${num_worker_nodes})) -lt 3 ]; then
		echo "Node count less than 3, creating minimal cluster and using directory-based OSDs"
		wget https://raw.githubusercontent.com/rook/rook/v${rook_version}/cluster/examples/kubernetes/ceph/cluster-test.yaml
	else
		wget https://raw.githubusercontent.com/rook/rook/v${rook_version}/cluster/examples/kubernetes/ceph/cluster.yaml
		if [ "${use_directories_for_osds}" == "true" ]; then
			echo "Using directory-based OSDs"
			sed 's@^\( *\)#directories:@\1directories:@' -i cluster.yaml
			sed 's@^\( *\)#- path:@\1- path:@' -i cluster.yaml
		fi
	fi
	$${KUBECTL} apply -f cluster*.yaml
	sleep 30

	wget https://raw.githubusercontent.com/rook/rook/v${rook_version}/cluster/examples/kubernetes/ceph/storageclass.yaml
	if [ $((${num_controller_worker_nodes} + ${num_worker_nodes})) -lt 3 ]; then
		sed 's@^\( *\)size: \d$@\size: 1@' -i storageclass.yaml
	fi
	$${KUBECTL} apply -f storageclass.yaml
	sleep 30

	# Make rook-ceph-block the default storage class
	$${KUBECTL} patch storageclass rook-ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

	cat <<-EOF > ceph-dashboard-ingress.yaml
	apiVersion: extensions/v1beta1
	kind: Ingress
	metadata:
	  annotations:
	    kubernetes.io/ingress.class: traefik
	  name: ceph-dashboard-ingress
	  namespace: rook-ceph
	spec:
	  rules:
	  - host: ceph.${k8s_cluster_fqdn}
	    http:
	      paths:
	      - backend:
	          serviceName: rook-ceph-mgr-dashboard
	          servicePort: 8443
	        path: /
	EOF
	$${KUBECTL} apply -f ceph-dashboard-ingress.yaml

	# Wait until OSDs have been created before proceeding
	set +x
	osd_state="not_ready"
	while [[ $${osd_state} != "ready" ]]; do
		echo -n "Checking if rook-ceph-osd-0 is ready..."
		if $${KUBECTL} get pods -n rook-ceph | grep rook-ceph-osd-0 >/dev/null 2>&1; then
			osd_state="ready"
		fi
		if [[ $${state} != "ready" ]]; then
			echo "not yet. Sleeping for 5 seconds..."
			sleep 5
		fi
	done
	echo "IT'S READY!"
	set -x

	echo "Moving rook-ceph namespace into Rancher System project..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d '{"projectId":"'${rancher2_system_project_id}'"}' \
		${rancher2_api_url}/cluster/${rancher2_cluster_id}/namespaces/rook-ceph?action=move 2>/dev/null
}

function deploy_traefik
{
	if [ "${deploy_traefik}" = "no" ]; then
		return
	fi

	echo "Deploying the Traefik Ingress Controller..."

	cat <<-EOF > /root/kube/traefik-ingress.yaml
	apiVersion: v1
	kind: Namespace
	metadata:
	  labels:
	    app: traefik-ingress
	  name: traefik-ingress

	---

	apiVersion: v1
	kind: PersistentVolumeClaim
	metadata:
	  name: traefik-certs
	  namespace: traefik-ingress
	spec:
	  accessModes:
	  - ReadWriteOnce
	  resources:
	    requests:
	      storage: 1Gi

	---

	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: traefik-ingress-controller
	  namespace: traefik-ingress

	---

	kind: ClusterRole
	apiVersion: rbac.authorization.k8s.io/v1beta1
	metadata:
	  name: traefik-ingress-controller
	rules:
	  - apiGroups:
	      - ""
	    resources:
	      - services
	      - endpoints
	      - secrets
	    verbs:
	      - get
	      - list
	      - watch
	  - apiGroups:
	      - extensions
	    resources:
	      - ingresses
	    verbs:
	      - get
	      - list
	      - watch
	  - apiGroups:
	    - extensions
	    resources:
	    - ingresses/status
	    verbs:
	    - update

	---

	kind: ClusterRoleBinding
	apiVersion: rbac.authorization.k8s.io/v1beta1
	metadata:
	  name: traefik-ingress-controller
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: traefik-ingress-controller
	subjects:
	- kind: ServiceAccount
	  name: traefik-ingress-controller
	  namespace: traefik-ingress

	---

	kind: DaemonSet
	apiVersion: extensions/v1beta1
	metadata:
	  name: traefik-ingress-controller
	  namespace: traefik-ingress
	  labels:
	    k8s-app: traefik-ingress-lb
	spec:
	  template:
	    metadata:
	      labels:
	        k8s-app: traefik-ingress-lb
	        name: traefik-ingress-lb
	    spec:
	      serviceAccountName: traefik-ingress-controller
	      terminationGracePeriodSeconds: 60
	      containers:
	      - image: traefik
	        imagePullPolicy: Always
	        name: traefik-ingress-lb
	        args:
	        - --api
	        - --accesslog
	        - --insecureSkipVerify=true
	        - --kubernetes
	        - --kubernetes.ingressendpoint.publishedService=traefik-ingress/traefik-ingress-service
	        - --kubernetes.disablePassHostHeaders=false
	        - --logLevel=INFO
	        - --defaultentrypoints=http,https
	        - --entrypoints=Name:https Address::443 TLS
	        - --entrypoints=Name:http Address::80 Redirect.EntryPoint:https
	        - --acme
	        - --acme.acmeLogging=true
	        - --acme.storage=/certs/acme.json
	        - --acme.entryPoint=https
	        - --acme.dnsChallenge.provider=route53
	        - --acme.email=${letsencrypt_email}
	        - --acme.caServer=${letsencrypt_server}
	        - --acme.domains=*.${k8s_cluster_fqdn},${k8s_cluster_fqdn}
	        env:
	        - name: AWS_ACCESS_KEY_ID
	          value: ${aws_access_key}
	        - name: AWS_SECRET_ACCESS_KEY
	          value: ${aws_secret_key}
	        ports:
	        - name: http
	          containerPort: 80
	          protocol: TCP
	        - name: https
	          containerPort: 443
	          protocol: TCP
	        - name: admin
	          containerPort: 8080
	          protocol: TCP
	        resources: {}
	        securityContext:
	          capabilities:
	            add:
	            - NET_BIND_SERVICE
	            drop:
	            - ALL
	          procMount: Default
	        terminationMessagePath: /dev/termination-log
	        terminationMessagePolicy: File
	        volumeMounts:
	        - mountPath: /certs
	          name: traefik-certs
	      dnsPolicy: ClusterFirst
	      restartPolicy: Always
	      schedulerName: default-scheduler
	      securityContext: {}
	      serviceAccount: traefik-ingress-controller
	      serviceAccountName: traefik-ingress-controller
	      terminationGracePeriodSeconds: 60
	      volumes:
	      - name: traefik-certs
	        persistentVolumeClaim:
	          claimName: traefik-certs
	  updateStrategy:
	    type: OnDelete

	---

	apiVersion: v1
	kind: Service
	metadata:
	  name: traefik-ingress-service
	  namespace: traefik-ingress
	spec:
	  ports:
	  - name: web
	    port: 80
	    protocol: TCP
	    targetPort: 80
	  - name: https
	    port: 443
	    protocol: TCP
	    targetPort: 443
	  selector:
	    k8s-app: traefik-ingress-lb
	  sessionAffinity: None
	  type: LoadBalancer

	---

	kind: Service
	apiVersion: v1
	metadata:
	  name: traefik-dashboard
	  namespace: traefik-ingress
	spec:
	  selector:
	    k8s-app: traefik-ingress-lb
	  ports:
	  - port: 8080
	    name: dashboard

	---

	apiVersion: extensions/v1beta1
	kind: Ingress
	metadata:
	  name: traefik-dashboard-ingress
	  namespace: traefik-ingress
	  annotations:
	    kubernetes.io/ingress.class: traefik
	spec:
	  rules:
	  - host: traefik.${k8s_cluster_fqdn}
	    http:
	      paths:
	      - path: /
	        backend:
	          serviceName: traefik-dashboard
	          servicePort: 8080
	EOF

	$${KUBECTL} apply -f /root/kube/traefik-ingress.yaml

	echo "Moving traefik-ingress namespace into Rancher System project..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d '{"projectId":"'${rancher2_system_project_id}'"}' \
		${rancher2_api_url}/cluster/${rancher2_cluster_id}/namespaces/traefik-ingress?action=move 2>/dev/null
}

function enable_prometheus_monitoring
{
	# Should be done via terraform rancher2_cluster variable but race condition
	# prevents this currently so we wait until the end of cloud-init.
	# See: https://github.com/rancher/rancher/issues/19362
	echo "Disabling Prometheus monitoring for the cluster as whole that was enabled via Terraform rancher2_cluster"
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d '{}' \
		${rancher2_api_url}/clusters/${rancher2_cluster_id}?action=disableMonitoring 2>/dev/null

	sleep 10

	echo "Re-enable Prometheus monitoring for the cluster as whole..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d '{}' \
		${rancher2_api_url}/clusters/${rancher2_cluster_id}?action=enableMonitoring 2>/dev/null

	sleep 60

	echo "Enabling Prometheus monitoring for the System project..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d "{}" \
		${rancher2_api_url}/projects/${rancher2_system_project_id}?action=enableMonitoring 2>/dev/null

	sleep 30

	echo "Enabling Prometheus monitoring for the Default project..."
	curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H "Accept: application/json" \
		-H "Content-Type: application/json" -d "{}" \
		${rancher2_api_url}/projects/${rancher2_default_project_id}?action=enableMonitoring 2>/dev/null
}

allow_root_login_for_cloud_init
install_kube_tools
install_docker
connect_to_rancher
get_kubeconfig
deploy_packet_csi
deploy_metal_lb
deploy_ceph
deploy_traefik
enable_prometheus_monitoring

touch /tmp/finished-user-data
