#!/bin/bash

K8S_CLUSTER_NAME="$(basename $(dirname $(pwd)))"

cat <<EOF > vars.tf

variable "infrastructure_provider" {
  default     = "${INFRASTRUCTURE_PROVIDER:-packet}"
  description = "Provider of underlying nodes"
}

variable "num_controller_worker_nodes" {
  default     = "1"
  description = "Number of controller/worker nodes"
}

variable "num_worker_nodes" {
  default     = "0"
  description = "Number of worker-only nodes"
}

variable "k8s_cluster_name" {
  description = "Name of your cluster. Alpha-numeric and hyphens only, please."
  default     = "${K8S_CLUSTER_NAME}"
}

variable "k8s_cluster_fqdn" {
  description = "FQDN of K8s cluster"
  default     = "${K8S_CLUSTER_NAME}.k8s.${INFRASTRUCTURE_PROVIDER:-packet}.${AWS_ZONE}"
}

variable "rook_version" {
  description = "Rook version to use for deploying Ceph"
  default     = "1.0.4"
}

variable "metal_lb_version" {
  description = "Meatal LB version"
  default     = "0.8.1"
}

variable "deploy_packet_csi" {
  description = "Deploy Packet CSI Storage Class"
  default     = "yes"
}

variable "deploy_metal_lb" {
  description = "Deploy Metal LB"
  default     = "yes"
}

variable "deploy_ceph" {
  description = "deploy ceph via rook"
  default     = "yes"
}

variable "use_directories_for_osds" {
  description = "Use local directories for Ceph OSDs instead of block devices"
  default     = "true"
}

variable "deploy_traefik" {
  description = "Deploy Traefik Ingress Controller"
  default     = "yes"
}

variable "packet_plan_controller" {
  description = "Packet plan for K8s controller nodes"
  default     = "baremetal_0"
}

variable "packet_plan_worker" {
  description = "Packet plan for K8s worker nodes"
  default     = "baremetal_0"
}

variable "packet_auth_token" {
  default = "${PACKET_AUTH_TOKEN}"
}

variable "packet_facility" {
  description = "Packet Facility"
	default     = "${PACKET_FACILITY:-ewr1}"
}

variable "packet_project_id" {
	default = "${PACKET_PROJECT_ID}"
}

variable "rancher2_api_url" {
  description = "Rancher2 API URL"
	default     = "${RANCHER_API_URL}"
}

variable "rancher2_access_key" {
  description = "Rancher2 Access Key"
  default     = "${RANCHER_ACCESS_KEY}"
}

variable "rancher2_secret_key" {
  description = "Rancher2 Secret Key"
  default     = "${RANCHER_SECRET_KEY}"
}

variable "rancher2_bearer_token" {
  description = "Rancher2 Bearer Token"
	default     = "${RANCHER_ACCESS_KEY}:${RANCHER_SECRET_KEY}"
}

variable "namecheap_api_user" {
  description = "Namecheap DNS API User"
  default     = "${NAMECHEAP_API_USER}"
}

variable "namecheap_api_key" {
  description = "Namecheap DNS API User"
  default     = "${NAMECHEAP_API_KEY}"
}

variable "aws_zone" {
  description = "Domain of AWS Route53 Zone to manipulate"
	default     = "${AWS_ZONE}."
}

variable "aws_region" {
  description = "AWS Region"
	default     = "${AWS_REGION:-us-east-1}"
}

variable "aws_access_key" {
  description = "AWS Access Key"
  default     = "${AWS_ACCESS_KEY}"
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
  default     = "${AWS_SECRET_KEY}"
}

variable "letsencrypt_server" {
  description = "LetsEncrypt Production or Staging Server"
  default     = "${LETSENCRYPT_SERVER:-https://acme-v02.api.letsencrypt.org/directory}"
}

variable "letsencrypt_email" {
  description = "LetsEncrypt email address to use for registration"
  default     = "${LETSENCRYPT_EMAIL}"
}
EOF
