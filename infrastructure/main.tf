terraform {
	required_version = ">= 0.12.2"
}

provider "null" {}

provider "packet" {
	version    = ">= 2.2.1"
	auth_token = "${var.packet_auth_token}"
}

provider "rancher2" {
	api_url    = "${var.rancher2_api_url}"
	access_key = "${var.rancher2_access_key}"
	secret_key = "${var.rancher2_secret_key}"
}

provider "aws" {
	region     = "${var.aws_region}"
	access_key = "${var.aws_access_key}"
	secret_key = "${var.aws_secret_key}"
}

resource "packet_reserved_ip_block" "kubernetes" {
	project_id = "${var.packet_project_id}"
	facility   = "${var.packet_facility}"
	quantity   = 2
}

data "aws_route53_zone" "zone" {
	# The trailing . is intentional when specifying DNS zones
  name         = "${var.aws_zone}"
  private_zone = false
}

resource "aws_route53_record" "elastic" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "${var.k8s_cluster_name}.k8s.${var.infrastructure_provider}.${data.aws_route53_zone.zone.name}"
  type    = "A"
  ttl     = "60"
  records = [cidrhost("${packet_reserved_ip_block.kubernetes.cidr_notation}", 0)]
	allow_overwrite = true
}

resource "aws_route53_record" "wildcard-elastic" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "*.${var.k8s_cluster_name}.k8s.${var.infrastructure_provider}.${data.aws_route53_zone.zone.name}"
  type    = "A"
  ttl     = "60"
  records = [cidrhost("${packet_reserved_ip_block.kubernetes.cidr_notation}", 0)]
	allow_overwrite = true
}

resource "rancher2_cluster" "cluster" {
	name = "${var.k8s_cluster_name}"
	description = "${var.k8s_cluster_name} on ${var.infrastructure_provider}"
	# Ideally we would just enable cluster monitoring here but there is a race
	# condition that is being worked on: https://github.com/rancher/rancher/issues/19362
	# We enable it anyway because that is the ultimate state that is desired, then
	# we will disable and re-enable in cloud-init via API calls
	enable_cluster_monitoring = true
	rke_config {
		#kubernetes_version = "v1.15.0-rancher1-1"
		#kubernetes_version = "v1.14.3-rancher1-1"
		network {
			plugin = "canal"
		}
		ingress {
			provider = "none"
		}
		services {
			kubelet {
				extra_args = {
					volume-plugin-dir = "/usr/libexec/kubernetes/kubelet-plugins/volume/exec"
				}
				extra_binds = [
					"/lib/modules:/lib/modules",
					"/var/lib/kubelet/plugins:/var/lib/kubelet/plugins",
					"/var/lib/kubelet/pods:/var/lib/kubelet/pods:shared,z",
					"/usr/libexec/kubernetes/kubelet-plugins/volume/exec:/usr/libexec/kubernetes/kubelet-plugins/volume/exec"
				]
			}
		}
	}
	cluster_auth_endpoint {
		enabled = true
	}
}

data "template_file" "controller" {
	template = "${file("node_config.tpl")}"

	vars = {
		k8s_cluster_fqdn            = "${var.k8s_cluster_fqdn}"
		packet_network_cidr         = "${packet_reserved_ip_block.kubernetes.cidr_notation}"
		packet_auth_token           = "${var.packet_auth_token}"
		packet_project_id           = "${var.packet_project_id}"
		aws_access_key              = "${var.aws_access_key}"
		aws_secret_key              = "${var.aws_secret_key}"
		num_controller_worker_nodes = "${var.num_controller_worker_nodes}"
		num_worker_nodes            = "${var.num_worker_nodes}"
		deploy_packet_csi           = "${var.deploy_packet_csi}"
		deploy_metal_lb             = "${var.deploy_metal_lb}"
		deploy_ceph                 = "${var.deploy_ceph}"
		deploy_traefik              = "${var.deploy_traefik}"
		rook_version                = "${var.rook_version}"
		use_directories_for_osds    = "${var.use_directories_for_osds}"
		metal_lb_version            = "${var.metal_lb_version}"
		rancher2_api_url            = "${var.rancher2_api_url}"
		rancher2_access_key         = "${var.rancher2_access_key}"
		rancher2_secret_key         = "${var.rancher2_secret_key}"
		rancher2_cluster_id         = "${rancher2_cluster.cluster.id}"
		rancher2_system_project_id  = "${rancher2_cluster.cluster.system_project_id}"
		rancher2_default_project_id = "${rancher2_cluster.cluster.default_project_id}"
		letsencrypt_server          = "${var.letsencrypt_server}"
		letsencrypt_email           = "${var.letsencrypt_email}"
		node_command                = "${rancher2_cluster.cluster.cluster_registration_token.0.node_command}"
	}
}

resource "packet_device" "k8s_controller_workers" {
	count            = "${var.num_controller_worker_nodes}"
	hostname         = "${format("${var.k8s_cluster_name}-controller-%02d",count.index)}"
	operating_system = "ubuntu_18_04"
	plan             = "${var.packet_plan_controller}"
	facilities       = ["${var.packet_facility}"]
	user_data        = "${data.template_file.controller.rendered}"
	tags             = ["kubernetes", "controller-${var.k8s_cluster_name}"]
	billing_cycle    = "hourly"
	project_id       = "${var.packet_project_id}"
	network_type     = "layer3"


	provisioner "remote-exec" {
		inline = [
			#"cloud-init status --wait"

			# Clever trick to tail cloud-init-output.log and bail out (sed 'q') only
			# when it comes to the last line in the cloud init script (which is touch
			# /tmp/finished-user-data) -- Because set -euxo nopipefail is set at the
			# top of the script, it will output the command having gotten there only
			# if nothing else failed
			"/bin/bash -c \"timeout 1200 sed '/finished-user-data/q' <(tail -f /var/log/cloud-init-output.log)\""
		]

		connection {
			type = "ssh"
			host = "${self.network.0.address}"
			user = "root"
			password = "${self.root_password}"
		}
	}

	lifecycle {
		#prevent_destroy = true
		ignore_changes = [ user_data ]
	}
}

resource "packet_ip_attachment" "kubernetes_lb_block" {
	device_id        = "${packet_device.k8s_controller_workers.0.id}"
	cidr_notation    = "${packet_reserved_ip_block.kubernetes.cidr_notation}"
}

resource "packet_device" "k8s_workers" {
	count            = "${var.num_worker_nodes}"
	hostname         = "${format("${var.k8s_cluster_name}-worker-%02d",count.index)}"
	operating_system = "ubuntu_18_04"
	plan             = "${var.packet_plan_worker}"
	facilities       = ["${var.packet_facility}"]
	user_data        = "${data.template_file.controller.rendered}"
	tags             = ["kubernetes", "worker-${var.k8s_cluster_name}"]
	billing_cycle    = "hourly"
	project_id       = "${var.packet_project_id}"
	network_type     = "layer3"
}

#resource "local_file" "kubeconfig" {
#	content = "${rancher2_cluster.cluster.kube_config}"
#	filename = ".terraform/kubeconfig"
#	depends_on = [packet_ip_attachment.kubernetes_lb_block]
#}

locals {
	kubernetes_server = "${replace(rancher2_cluster.cluster.kube_config, "/(?ms)^.*server: \"(https://rancher.+?)\".*$(?ms)/", "$1")}"
	kubernetes_server_direct = "${replace(rancher2_cluster.cluster.kube_config, "/(?ms)^.*server: \"(https://[\\d].+?)\".*$(?ms)/", "$1")}"
	kubernetes_token  = "${replace(rancher2_cluster.cluster.kube_config, "/(?ms)^.*token: \"(.+?)\".*$(?ms)/", "$1")}"
}

provider "kubernetes" {
	#config_path = ".terraform/kubeconfig"
	host = "${local.kubernetes_server}"
	token = "${local.kubernetes_token}"
	load_config_file = false
}

#resource "null_resource" "kubeconfig" {
#	triggers = {
#		filename = "kubeconfig"
#		#contents = "${fileexists("${path.module}/.terraform/kubeconfig") ? file("${path.module}/.terraform/kubeconfig") : ""}"
#	}
#	provisioner "local-exec" {
#		command =<<-EOF
#			curl -u "${var.rancher2_access_key}:${var.rancher2_secret_key}" \
#				-X POST \
#				-H 'Accept: application/json' \
#				-H 'Content-Type: application/json' \
#				-d '{}' \
#				"${var.rancher2_api_url}/clusters/${rancher2_cluster.cluster.id}?action=generateKubeconfig" 2>/dev/null | jq -r '.config' > ${self.triggers.filename}
#		EOF
#	}
#	depends_on = [
#		packet_ip_attachment.kubernetes_lb_block
#	]
#	#lifecycle {
#	#	ignore_changes = [
#	#		triggers.contents
#	#	]
#	#}
#}
#resource "null_resource" "kubeconfig_contents" {
#	triggers = {
#		contents = "${fileexists("kubeconfig") ? file("kubeconfig") : ""}"
#	}
#	depends_on = [
#		null_resource.kubeconfig
#	]
#}

data "template_file" "get_kubeconfig" {
	template = "${file("get_kubeconfig.sh")}"

	vars = {
		rancher2_api_url            = "${var.rancher2_api_url}"
		rancher2_access_key         = "${var.rancher2_access_key}"
		rancher2_secret_key         = "${var.rancher2_secret_key}"
		rancher2_cluster_id         = "${rancher2_cluster.cluster.id}"
	}
}

data "external" "kubeconfig" {
	count = "${fileexists("kubeconfig") ? 0 : 1}"
	program = [ "/bin/sh", "-c", "${data.template_file.get_kubeconfig.rendered}" ]
	depends_on = [
		packet_ip_attachment.kubernetes_lb_block
	]
}

#data "local_file" "kubeconfig" {
#	filename = "kubeconfig"
#	depends_on = [
#		data.external.kubeconfig
#	]
#}

#data "local_file" "kubeconfig" {
#	filename = "${null_resource.kubeconfig.triggers.filename}"
#	depends_on = [
#		null_resource.kubeconfig
#	]
#}

#data "null_data_source" "kubeconfig" {
#	inputs = {
#		data = "${file(null_resource.kubeconfig.triggers.filename)}"
#		null = "${format(null_resource.kubeconfig.id)}"
#	}
#	depends_on = [
#		null_resource.kubeconfig
#	]
#}


#resource "kubernetes_namespace" "metallb-system" {
#	metadata {
#		annotations = {
#			"field.cattle.io/projectId" = "${rancher2_cluster.cluster.system_project_id}"
#		}
#		name = "metallb-system"
#	}
#}
#
#resource "kubernetes_namespace" "rook-ceph" {
#	metadata {
#		annotations = {
#			"field.cattle.io/projectId" = "${rancher2_cluster.cluster.system_project_id}"
#		}
#		name = "rook-ceph"
#	}
#}
#
#resource "kubernetes_namespace" "traefik-ingress" {
#	metadata {
#		annotations = {
#			"field.cattle.io/projectId" = "${rancher2_cluster.cluster.system_project_id}"
#		}
#		name = "traefik-ingress"
#	}
#}

#resource "rancher2_project" "system" {
#	name = "System"
#	cluster_id = "${rancher2_cluster.cluster.id}"
#	enable_project_monitoring = true
#	wait_for_cluster = true
#}
#
#resource "rancher2_project" "default" {
#	name = "Default"
#	cluster_id = "${rancher2_cluster.cluster.id}"
#	enable_project_monitoring = true
#	wait_for_cluster = true
#}

output "controller_addresses" {
	description = "Kubernetes Controller IP Addresses"
	value       = "${packet_device.k8s_controller_workers.*.network.0.address}"
}
output "controller_root_password" {
	#sensitive   = true
	description = "Kubernetes Controller root password"
	value       = "${packet_device.k8s_controller_workers.*.root_password}"
}
output "worker_addresses" {
	description = "Kubernetes Worker IP Addresses"
	value       = "${packet_device.k8s_workers.*.network.0.address}"
}
output "workers_root_password" {
	#sensitive   = true
	description = "Kubernetes Workers root password"
	value       = "${packet_device.k8s_workers.*.root_password}"
}
output "kubernetes_fqdn" {
	#sensitive   = true
	description = "FQDN of the K8s cluster"
	value       = "${var.k8s_cluster_fqdn}"
}
output "kubeconfig" {
	#sensitive   = true
	description = "kubeconfig file"
	#value       = "${rancher2_cluster.cluster.kube_config}"
	#value        = "${file(".terraform/kubeconfig")}"
	#value       = "${fileexists(null_resource.kubeconfig.triggers.filename) ? file(null_resource.kubeconfig.triggers.filename) : null_resource.kubeconfig.triggers.filename}"
	value        = "${fileexists("kubeconfig") ? file("kubeconfig") : data.external.kubeconfig.0.result.config}"
	#value        = "${fileexists("kubeconfig") ? file("kubeconfig") : null_resource.kubeconfig_contents.triggers.contents}"
	#value        = "${data.null_data_source.kubeconfig.outputs["data"]}"
	#value        = "${data.external.kubeconfig.0.result.config}"
	#value        = "${null_resource.kubeconfig_file.triggers.contents}"
}
output "kubernetes_server" {
	#sensitive   = true
	description = "Address of the kube-apiserver"
	value       = "${local.kubernetes_server}"
}
output "kubernetes_server_direct" {
	#sensitive   = true
	description = "Direct address of the kube-apiserver not proxied through Rancher"
	value       = "${local.kubernetes_server_direct}"
}
output "kubernetes_token" {
	#sensitive   = true
	description = "Token to access the kube-apiserver"
	value       = "${local.kubernetes_token}"
}
