if [ ! -f kubeconfig ]; then
	output=$(curl -u "${rancher2_access_key}:${rancher2_secret_key}" -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{}' "${rancher2_api_url}/clusters/${rancher2_cluster_id}?action=generateKubeconfig" 2>/dev/null) #| tee /dev/null | jq -r '.config' > kubeconfig
	printf "%s" "$output" | jq -r '.config' > kubeconfig
else
	config=$(cat kubeconfig)
	output=$(jq -n --arg config "$config" '{"config":$config}')
fi
printf "%s" "$output"
